import copy
class FWSolution:
    def __init__(self,G):
        self.G = G
        self.path = []

    def computeAPSP(self):
        n = len(self.G)
        path1 = []
        for i in range(n + 1):
            path1.append([float("inf")] * (n + 1))
        path2 = []
        for i in range(n + 1):
            path2.append([float("inf")] * (n + 1))
        path = [path1, path2]
        
        for i in range(1, n + 1):
            for j in range(1, n + 1):
                if i == j: path[0][i][j] = 0
                else: path[0][i][j] = self.G[i].get(j, float("inf"))
        for k in range(1, n + 1):
            print k, 'iteration\r',
            for i in range(1, n + 1):
                for j in range(1, n + 1):
                    path[1][i][j] = min(path[0][i][j], path[0][i][k] + path[0][k][j])
            
            path[0], path[1] = path[1], path[0]
        for i in range(1, n + 1):
            if path[0][i][i] < 0:
                print 'Negative cycle exists'
                return None
        self.path = path[0]
        return 1
        
    def reconstructPath(self):
        return

if __name__ == "__main__":
    
    f = open('_6ff856efca965e8774eb18584754fd65_g3.txt')
    #f = open('_919f8ed0c52d8b5926aa7e3fdecc2d32_large.txt')
    [n,m] = [int(i) for i in f.readline().split()]
    G = {}
    for i in range(n):
        temp = {}
        G[i+1] = temp
    for line in f:
        G[int(line.split()[0])][int(line.split()[1])] = int(line.split()[2])
    print 'n, m is', n, m
    myAns = FWSolution(G)
    if myAns.computeAPSP():
        print '                              \r',
        print 'answer is', min(min(myAns.path))
    