class BFSolution():
    def __init__(self, G):
        self.G = G
        self.weight = []
    def positiviseEdges(self):
        n = len(self.G)
        temp = {}
        for i in range(1, n+1):
            temp[i] = 0
        self.G[0] = temp
        hasTail = []
        for i in range(n+1):
            hasTail.append(set())
        for i in range(n+1):
            for head in self.G[i]:
                hasTail[head].add(i)
                
                
        path = []
        path.append([0] * (n + 1))
        path.append([0] + [float("inf")] * n)
        for i in range(n + 1):
            print i,'iteration\r',
            for v in range(1, n + 1):
                path[1][v] = min(path[0][v], \
                                 min([path[0][w] + self.G[w][v] for w in hasTail[v]]))
            if path[0] == path[1]: break
            path[0],path[1] = path[1],path[0]
        if path[1] != path[0]:
            print 'Negative cycle exists'
            
            return None
        else:
            for v in range(1, n + 1):
                for head in self.G[v]:
                    self.G[v][head] += path[0][v] - path[0][head]
            self.weight = path[0]
            self.G.pop(0)
            print 'finished modifing the graph'
            return 1
            
        
            