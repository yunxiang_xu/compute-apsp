from Dijkstra import DijkstraSolution
from BellmanFord import BFSolution
if __name__ == "__main__":
    
    f = open('_6ff856efca965e8774eb18584754fd65_g3.txt')
    #f = open('_919f8ed0c52d8b5926aa7e3fdecc2d32_large.txt')
    [n,m] = [int(i) for i in f.readline().split()]
    G = {}
    for i in range(n):
        temp = {}
        G[i+1] = temp
    for line in f:
        G[int(line.split()[0])][int(line.split()[1])] = int(line.split()[2])
    myAns = BFSolution(G)
    if myAns.positiviseEdges():
    
        for v in range(1, n+1):
            for head in myAns.G[v]:
                if myAns.G[v][head] < 0:
                    print v,head
        myAns2 = DijkstraSolution(myAns.G)
        ansArray = []
        ans = float("inf")
       
        for i in range(1, n + 1):
            print i, 'computing\r',
            myAns2.ComputeShortestPath(i)
            ansArray = [myAns2.keyValue[i] + myAns.weight[j] - myAns.weight[i] for j in range(1, n + 1)]
            ans = min(ans,min(ansArray))
        print "                 \r",
        print 'answer:',ans
    
    